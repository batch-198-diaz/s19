// index.js

console.log("Hello World");

// CONDITIONAL STATEMENTS

// Conditional statements allow us to perform tasks based on a condition.

let num1 = 0;

/*
	IF statement - runs code if the condition given is true

		if(condition){
			<insert task/code to perform>
		}
*/

if (num1 === 0){
	console.log("The value of num1 is 0");
}

num1 = 25

if (num1 === 0){
	console.log("The value of num1 is 0");
}

let city = "New York";

if(city=== "New Jersey"){
	console.log("Welcome to New Jersey!");
}

// ELSE


if(city=== "New Jersey"){
	console.log("Welcome to New Jersey!");
} else {
	console.log("This is not New Jersey!");
}

if(num1 < 20){
	console.log("num1's value is less than 20");
} else {
	console.log("num1's value is more than 20");
}

// Can we use if-else in a function? Yes. It improves reusability.

function cityChecker(city){

	if(city === "New York"){
		console.log("Welcome to the Empire State!");
	} else {
		console.log("You're not in New York!");
	}

}

cityChecker("New York");
cityChecker("Los Angeles");


/* 

*/

function budget(num){
	if(num<=40000){
		console.log("You're still within budget.");
	} else {
		console.log("You are currently over budget.");
	}
}

budget(5000);
budget(55000);



// ELSE IF
//execute code if previous condition/s are not met or false and if specified condition is met instead

let city2 = "Manila";

if(city2 === "New York"){
	console.log("Welcome to New York!");
} else if(city2 === "Manila"){
	console.log("Welcome to Manila!");
} else {
	console.log("I don't know where you are.");
}

//Else and else if statements are optional - i.e. in a conditional chain/statement, there should always be an if statement, however you can skip or not add else if or else statements.

if(city2 === "New York"){
	console.log("New York City!");
} 
// else if(city2 === "Manila"){
// 	console.log("I keep coming back to Manila!");
// } 
else {
	console.log("Where's Waldo?");
}


//else is always last and cannot go before else if
let role = "admin";

if (role === "developer"){
	console.log("Welcome back, developer.");
} else if(role === "admin") {
	console.log("Hello, Admin");
} else {
	console.log("Role provided is invalid.");
}


// Multiple Else If Statements

function determiineTyphoonIntensity(windSpeed){

	if(windSpeed < 30){
		return "Not a typhoon yet.";
	} else if(windSpeed <= 60){
		return "Tropical Depression Detected.";
	} else if(windSpeed >= 62 && windSpeed <= 88){
		return "Tropical Storm Detected.";
	} else if(windSpeed >= 89 && windSpeed <= 117){
		return "Severe Tropical Storm Detected.";
	} else {
		return "Typhoon Detected.";
	}

}

let typhoonMessage1 = determiineTyphoonIntensity(29);
let typhoonMessage2 = determiineTyphoonIntensity(62);
let typhoonMessage3 = determiineTyphoonIntensity(61);
let typhoonMessage4 = determiineTyphoonIntensity(88);
let typhoonMessage5 = determiineTyphoonIntensity(117);
let typhoonMessage6 = determiineTyphoonIntensity(120);

console.log(typhoonMessage1);
console.log(typhoonMessage2);
console.log(typhoonMessage3);
console.log(typhoonMessage4);
console.log(typhoonMessage5);
console.log(typhoonMessage6);


// Truthy and Falsy Values

//In JS, there are values that are considered "truthy", which means in a boolean context, like determining an if condition, it is considered true.

/*
	Samples of Truthy
	- true
	- "1"
	- empty array
*/

if(1){
	console.log("1 is truthy.");
}

if([]){
	console.log("[] empty array is truthy.")
} //even though empty, the array is already an existing instance

/*
	Samples of Falsy
	- false
	- "0"
	- "undefined"
*/

if(0){
	console.log("0 is falsy.");
}

if(undefined){
	console.log("undefined is not falsy")
} else {
	console.log("undefined is falsy")
}


// Conditional Ternary Operator

// Ternary operator is used as a shorter alternative to if else statements. It is also able to implicitly return a value i.e. it does not have to use the return keyword to return a value. Only used as a shorthand for if else but cannot have more than two conditions.

/*
	Syntax: 
	(condition) ? ifTrue : ifFalse;
*/

let age = 17;
let result = age < 18 ? "Underage." : "Legal Age."
console.log(result);

	// let result2 = if(age < 18){
	// 	return "Underage";
	// } else {
	// 	return "Legal Age";
	// }

	// console.log(result2); 
	//error


// SWITCH STATEMENTS

/*

- Evaluate an expression and match the expression to a case clause.
- An expression will be compared against different cases. Then, we will be able to run code IF the expression being evaluated matches a case. 
- It is used alternatively from an if-else statement. However, if-else statements provides more complexity in its conditions

*/

let day = prompt("What day of the week is it today?").toLowerCase();
//[.toLowercase] method is a built-in JS method which converts a string to lower case.
console.log(day);

//Switch statement to evaluate the current day and show a message of the color of the day.

/*
	Mini-Activity
		
	Add the remaining days as cases for our switch statement
		-thursday
		-friday
		-saturday
		-sunday
	You can customize the message per case that will be logged in the console.
	Take a screenshot of your console for at leastt 1 of the new cases.
*/

switch(day) {
	case 'monday':
		console.log("The color of the day is red.");
		break;
	case 'tuesday':
		console.log("The color of the day is orange.");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow.");
		break;
	case 'thursday':
		console.log("The color of the day is green.");
		break;
	case 'friday':
		console.log("The color of the day is blue.");
		break;
	case 'saturday':
		console.log("The color of the day is indigo.");
		break;
	case 'sunday':
		console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please enter a valid day");
		break;
}


// TRY-CATCH-FINALLY

// It is used to anticipate, catch and inform about an error i.e. it catches errors, displays and informs about the error and the code still continues instead of stopping. We could also use this to produce our own error message in the event of an eror. 

/*
- Try allows us to run code, if there is an error in the instance of our code, then we will be able to catch that error in our catch statement.
- The error is passed into the catch statement.. developers usually name the error as error, err or even e.
- With the use of typeof keyword, we will be able to return string which contains information to what data type our error is.
- error is an object with methods and properties that describe the error
- error.message allows us to access information about the error
- try-catch statement can catch errors and instead of stopping the whole program, it continues.
- Finally statement will run regardless of the success or failure of the try statement.
*/

try{

	alerts(determiineTyphoonIntensity(50))

} catch(error) {

	console.log(typeof error);
	console.log(error);
	console.log(error.message);

} finally {

	alert("Intensity updates will show in a new alert");
}

console.log("We will continue to the next code?");