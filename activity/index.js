// index.js - activity

console.log("Hello World");


//USERNAME

let username = prompt("Input username?").toLowerCase();

if(username.length === 0 || username === null){
	alert("Username should not be empty.");
}

//PASSWORD
let password = prompt("Input password?").toLowerCase();

if(password.length === 0 || password === null){
	alert("Password should not be empty.");
}

//ROLE
let role = prompt("Input role?").toLowerCase();

if(role.length === 0 || role === null){
	alert("Password should not be empty.");
} else {
	switch(role) {
		case 'admin':
			alert("Welcome back to the class portal, admin!");
			break;	
		case 'teacher':
			alert("Thank you for logging in, teacher!");
			break;
		case 'rookie':
			alert("Welcome to the class portal, student!");
			break;
		default:
			alert("Role out of range.");
			break;
	}
}

//GRADE AVERAGE EQUIVALENT

function checkAverage(num_1,num_2,num_3,num_4){
	let sum = num_1 + num_2 + num_3 + num_4;
	let average = sum / 4;
	average = Math.round(average);
	//console.log(average);

	if(average <= 74){
		console.log("Hello, student, your average is " + average +". The letter equivalent is F.");
	} else if(average >= 75 && average <=79){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D.");
	} else if(average >= 80 && average <=84){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C.");
	} else if(average >= 85 && average <=89){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B.");
	} else if(average >= 90 && average <=95){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A.");
	} else if(average >= 96){
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+.");
	} else {
		console.log("Cannot compute");
	}

}


let grades_1 = checkAverage(70,70,72,71);
let grades_2 = checkAverage(76,76,77,79);
let grades_3 = checkAverage(81,83,84,85);
let grades_4 = checkAverage(87,88,88,89);
let grades_5 = checkAverage(91,90,92,90);
let grades_6 = checkAverage(96,95,97,97);


console.log(grades_1);
console.log(grades_2);
console.log(grades_3);
console.log(grades_4);
console.log(grades_5);
console.log(grades_6);